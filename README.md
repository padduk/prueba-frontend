## Prueba Frontend
Este proyecto esta basado en el bootstrap code de [create-react-app](https://github.com/facebookincubator/create-react-app).

## Instalar dependencias
```bash
## Instalar dependencias usando NPM
[/path/to/prueba-frontend] $ npm install  

## Instalar dependencias usando YARN
[/path/to/prueba-frontend] $ yarn install
```

## Iniciar el proyecto
```bash
[/path/to/prueba-frontend] $ npm start  
```

Este comando abre una nueva pestaña en tu navegador con el preview del proyecto. Este está escuchando cambios en el código y refresca automáticamente cambios en el navegador.  
