import React, { Component } from 'react'
import logo from './logo.svg'

/* Styles */
import './App.css'

/* Material UI */
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { grey900, yellow500 } from 'material-ui/styles/colors'

import RaisedButton from 'material-ui/RaisedButton'

class App extends Component {
  constructor(props) {
    super(props)

    /* STATE */
    this.state = {}

    this.getTheme = this.getTheme.bind(this)
  }

  getTheme () {
    return getMuiTheme({
      palette: {
        primary1Color: yellow500,
        accent1Color: grey900,
      }
    }, {
      userAgent: 'all',
      avatar: {
        borderColor: null,
      }
    })
  }

  render() {
    return <MuiThemeProvider muiTheme={this.getTheme()}>
      <div className="App">
        <div className="App-header" style={{ background: yellow500 }}>
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Prueba Frontend</h2>
        </div>
        <p className="App-intro">
          Para iniciar, modifica <code>src/App.js</code> guarda y esta página recarga automáticamente.
        </p>

        <RaisedButton label={'Hello world'} secondary={true} />
      </div>
    </MuiThemeProvider>
  }
}

export default App
